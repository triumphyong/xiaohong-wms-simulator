export interface MessageReceived {
  plcClienttIp: string
  plcClientPort: number
  plcServerPort: number
  scanPort: number
}

export interface MessageSend {
  plcClientStatus: boolean
  plcServerStatus: boolean
  plcLogMessage: string
  scanStatus: boolean
  scanLogMessage: string
}
