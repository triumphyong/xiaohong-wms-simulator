import { Result } from '@/types/result'
import request from '@/utils/request'

/**
 * 创建服务端
 * @param port 服务端端口号
 * @returns
 */
export const creatServer = (port: number, path: string) => {
  return request.get<Result<String>>('/socket/websocket/server/create', {
    params: {
      port,
      path
    },
  })
}

/**
 * 关闭所有服务端
 * @returns
 */
export const closeServer = () => {
  return request.get<Result<String>>('/socket/websocket/server/close')
}

/**
 * 创建客户端
 * @param ip 客户端ip
 * @param port 客户端端口
 * @returns
 */
export const creatClient = (ip: string, port: number) => {
  return request.get<Result<String>>('/socket/websocket/client/create', {
    params: {
      ip,
      port,
    },
  })
}

/**
 * 关闭所有客户端
 * @returns
 */
export const closeClient = () => {
  return request.get<Result<String>>('/socket/websocket/client/close')
}

/**
 * 服务端发送消息
 * @returns
 */
export const sendMessageByServer = (data: any) => {
  return request.post<Result<String>>('/socket/websocket/server/send', data)
}

/**
 * 客户端发送消息
 * @param data 消息数据
 * @returns
 */
export const sendMessageByClient = (data: any) => {
  return request.post<Result<String>>('/socket/websocket/client/send', data)
}
