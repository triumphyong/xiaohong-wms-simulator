import { setItem } from '@/utils/storage'
import { defineStore } from 'pinia'

export const scanStore = defineStore('scanStore', {
  state: () => {
    return {
      scanClientIp: localStorage.getItem('scanClientIp') || '127.0.0.1',
      scanClientPort: parseInt(localStorage.getItem('scanClientPort')) || null,
      scanServerPort: parseInt(localStorage.getItem('scanServerPort')) || null,
      scanClientStatus: localStorage.getItem('scanClientStatus') === 'true',
      scanServerStatus: localStorage.getItem('scanServerStatus') === 'true',
      scanLog: JSON.parse(sessionStorage.getItem('scanLog')) as Array<String> || [] as Array<String>
    }
  },
  actions: {
    setScanLog (payLoad:string) {
      if (this.scanLog.length >= 200) {
        this.scanLog.shift()
      }
      this.scanLog.push(payLoad)
      sessionStorage.setItem('scanLog', JSON.stringify(this.scanLog))
    },
    setScanClientStatus (payLoad:boolean) {
      this.scanClientStatus = payLoad
      setItem('scanClientStatus', JSON.stringify(payLoad))
    },
    setScanServerStatus (payLoad:boolean) {
      this.scanServerStatus = payLoad
      setItem('scanServerStatus', JSON.stringify(payLoad))
    },
    setScanClientIp (payLoad:string) {
      this.scanClientIp = payLoad
      setItem('scanClientIp', payLoad)
    },
    setScanClientPort (payLoad:number) {
      this.scanClientPort = payLoad
      setItem('scanClientPort', JSON.stringify(payLoad))
    },
    setScanServerPort (payLoad:number) {
      this.scanServerPort = payLoad
      setItem('scanServerPort', JSON.stringify(payLoad))
    }
  }
})
