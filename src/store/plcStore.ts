import { setItem } from '@/utils/storage'
import { defineStore } from 'pinia'

export const plcStore = defineStore('plcStore', {
  state: () => {
    return {
      plcClientIp: localStorage.getItem('plcClienttIp') || '127.0.0.1',
      plcClientPort: parseInt(localStorage.getItem('plcClientPort')) || null,
      plcServerPort: parseInt(localStorage.getItem('plcServerPort')) || null,
      plcClientStatus: localStorage.getItem('plcClientStatus') === 'true',
      plcServerStatus: localStorage.getItem('plcServerStatus') === 'true',
      plcLog: JSON.parse(sessionStorage.getItem('plcLog')) as Array<String> || [] as Array<String>
    }
  },
  actions: {
    setPlcLog (payLoad:string) {
      if (this.plcLog.length >= 200) {
        this.plcLog.shift()
      }
      this.plcLog.push(payLoad)
      sessionStorage.setItem('plcLog', JSON.stringify(this.plcLog))
    },
    setPlcClientStatus (payLoad:boolean) {
      this.plcClientStatus = payLoad
      setItem('plcClientStatus', JSON.stringify(payLoad))
    },
    setPlcServerStatus (payLoad:boolean) {
      this.plcServerStatus = payLoad
      setItem('plcServerStatus', JSON.stringify(payLoad))
    },
    setPlcClientIp (payLoad:string) {
      this.plcClienttIp = payLoad
      setItem('plcClienttIp', payLoad)
    },
    setPlcClientPort (payLoad:number) {
      this.plcClientPort = payLoad
      setItem('plcClientPort', JSON.stringify(payLoad))
    },
    setPlcServerPort (payLoad:number) {
      this.plcServerPort = payLoad
      setItem('plcServerPort', JSON.stringify(payLoad))
    }
  }
})
