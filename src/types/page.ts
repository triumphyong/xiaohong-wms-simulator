export interface Page<T> {
  records: Array<T>
  total: number
  size: number
  current: number
  orders: Array<boolean>
  optimizeCountSql: boolean
  searchCount: boolean
  optimizeJoinOfCountSql: boolean
  countId: string
  maxLimit: number
  pages: number
}
