import { setItem } from '@/utils/storage'
import { defineStore } from 'pinia'

export const generalStore = defineStore('generalStore', {
  state: () => {
    return {
      generalClientIp: localStorage.getItem('generalClientIp') || '127.0.0.1',
      generalClientPort: parseInt(localStorage.getItem('generalClientPort')) || null,
      generalServerPort: parseInt(localStorage.getItem('generalServerPort')) || null,
      generalClientStatus: localStorage.getItem('generalClientStatus') === 'true',
      generalServerStatus: localStorage.getItem('generalServerStatus') === 'true',
      generalLog: JSON.parse(sessionStorage.getItem('generalLog')) as Array<String> || [] as Array<String>
    }
  },
  actions: {
    setGeneralLog (payLoad:string) {
      if (this.generalLog.length >= 200) {
        this.generalLog.shift()
      }
      this.generalLog.push(payLoad)
      sessionStorage.setItem('generalLog', JSON.stringify(this.generalLog))
    },
    setGeneralClientStatus (payLoad:boolean) {
      this.generalClientStatus = payLoad
      setItem('generalClientStatus', JSON.stringify(payLoad))
    },
    setGeneralServerStatus (payLoad:boolean) {
      this.generalServerStatus = payLoad
      setItem('generalServerStatus', JSON.stringify(payLoad))
    },
    setGeneralClientIp (payLoad:string) {
      this.generalClientIp = payLoad
      setItem('generalClientIp', payLoad)
    },
    setGeneralClientPort (payLoad:number) {
      this.generalClientPort = payLoad
      setItem('generalClientPort', JSON.stringify(payLoad))
    },
    setGeneralServerPort (payLoad:number) {
      this.generalServerPort = payLoad
      setItem('generalServerPort', JSON.stringify(payLoad))
    }
  }
})
