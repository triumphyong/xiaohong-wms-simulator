import { createApp } from 'vue'
import App from '@/App.vue'
import elementPlus from './plugins/element-plus'
import i18n from './plugins/i18n'
import pinia from './plugins/pinia'
import router from '@/router'
import '@/styles/index.scss' // 加载全局样式
import 'virtual:svg-icons-register'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import SvgIcon from '@/components/SvgIcon/index.vue'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'
import '@/assets/iconfont/iconfont.css' // 引入字体图标

const app = createApp(App)
app.use(pinia)
// 注册所有图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}

app.use(Antd)

app.component('SvgIcon', SvgIcon)

app.use(elementPlus)
app.use(i18n)
app.use(router)
app.mount('#app')
