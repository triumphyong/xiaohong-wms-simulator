import { setItem } from '@/utils/storage'
import { defineStore } from 'pinia'

export const ikeaStore = defineStore('ikeaStore', {
  state: () => {
    return {
      ikeaClientIp: localStorage.getItem('ikeaClienttIp') || '127.0.0.1',
      ikeaClientPort: parseInt(localStorage.getItem('ikeaClientPort')) || null,
      ikeaServerPort: parseInt(localStorage.getItem('ikeaServerPort')) || null,
      ikeaClientStatus: localStorage.getItem('ikeaClientStatus') === 'true',
      ikeaServerStatus: localStorage.getItem('ikeaServerStatus') === 'true',
      ikeaLog: JSON.parse(sessionStorage.getItem('ikeaLog')) as Array<String> || [] as Array<String>
    }
  },
  actions: {
    setIkeaLog (payLoad:string) {
      if (this.ikeaLog.length >= 200) {
        this.ikeaLog.shift()
      }
      this.ikeaLog.push(payLoad)
      sessionStorage.setItem('ikeaLog', JSON.stringify(this.ikeaLog))
    },
    setIkeaClientStatus (payLoad:boolean) {
      this.ikeaClientStatus = payLoad
      setItem('ikeaClientStatus', JSON.stringify(payLoad))
    },
    setIkeaServerStatus (payLoad:boolean) {
      this.ikeaServerStatus = payLoad
      setItem('ikeaServerStatus', JSON.stringify(payLoad))
    },
    setIkeaClientIp (payLoad:string) {
      this.ikeaClienttIp = payLoad
      setItem('ikeaClienttIp', payLoad)
    },
    setIkeaClientPort (payLoad:number) {
      this.ikeaClientPort = payLoad
      setItem('ikeaClientPort', JSON.stringify(payLoad))
    },
    setIkeaServerPort (payLoad:number) {
      this.ikeaServerPort = payLoad
      setItem('ikeaServerPort', JSON.stringify(payLoad))
    }
  }
})
