import { setItem } from '@/utils/storage'
import { defineStore } from 'pinia'

export const agvServerStore = defineStore('agvServerStore', {
  state: () => {
    return {
      agvServerClientIp: localStorage.getItem('agvServerClientIp') || '127.0.0.1',
      agvServerClientPort: parseInt(localStorage.getItem('agvServerClientPort')) || null,
      agvServerServerPort: parseInt(localStorage.getItem('agvServerServerPort')) || null,
      agvServerClientStatus: localStorage.getItem('agvServerClientStatus') === 'true',
      agvServerServerStatus: localStorage.getItem('agvServerServerStatus') === 'true',
      agvServerLog: JSON.parse(sessionStorage.getItem('agvServerLog')) as Array<String> || [] as Array<String>
    }
  },
  actions: {
    setAgvServerLog (payLoad:string) {
      if (this.agvServerLog.length >= 200) {
        this.agvServerLog.shift()
      }
      this.agvServerLog.push(payLoad)
      sessionStorage.setItem('agvServerLog', JSON.stringify(this.agvServerLog))
    },
    setAgvServerClientStatus (payLoad:boolean) {
      this.agvServerClientStatus = payLoad
      setItem('agvServerClientStatus', JSON.stringify(payLoad))
    },
    setAgvServerServerStatus (payLoad:boolean) {
      this.agvServerServerStatus = payLoad
      setItem('agvServerServerStatus', JSON.stringify(payLoad))
    },
    setAgvServerClientIp (payLoad:string) {
      this.agvServerClientIp = payLoad
      setItem('agvServerClientIp', payLoad)
    },
    setAgvServerClientPort (payLoad:number) {
      this.agvServerClientPort = payLoad
      setItem('agvServerClientPort', JSON.stringify(payLoad))
    },
    setAgvServerServerPort (payLoad:number) {
      this.agvServerServerPort = payLoad
      setItem('agvServerServerPort', JSON.stringify(payLoad))
    }
  }
})
