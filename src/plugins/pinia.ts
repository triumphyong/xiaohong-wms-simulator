import { createPinia } from 'pinia'
import { createApp } from 'vue'
import App from '@/App.vue'

const app = createApp(App)
const pinia = createPinia()
app.use(pinia)

export default pinia
