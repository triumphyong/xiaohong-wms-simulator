export interface RobokitMessage {
  messageType: number,
  messageSerial: number,
  message: string
 }
