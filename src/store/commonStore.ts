import { setItem } from '@/utils/storage'
import { defineStore } from 'pinia'

export const commonStore = defineStore('commonStore', {
  state: () => {
    return {
      isCollapse: false,
      language: 'en',
      wsStatus: false
    }
  },
  // could also be defined as
  // state: () => ({ count: 0 })
  actions: {
    setCollapse (payLoad:boolean) {
      this.isCollapse = payLoad
    },
    setLanguage (payLoad:string) {
      this.language = payLoad
    },
    setWsStatus (payLoad:boolean) {
      this.wsStatus = payLoad
    },
    setPlcLog (payLoad:string) {
      this.plcLog = payLoad
      setItem('plcLog', JSON.stringify(payLoad))
    }
  }
})
