import { setItem } from '@/utils/storage'
import { defineStore } from 'pinia'

export const stripStore = defineStore('stripStore', {
  state: () => {
    return {
      stripClientIp: localStorage.getItem('stripClientIp') || '127.0.0.1',
      stripClientPort: parseInt(localStorage.getItem('stripClientPort')) || null,
      stripServerPort: parseInt(localStorage.getItem('stripServerPort')) || null,
      stripClientStatus: localStorage.getItem('stripClientStatus') === 'true',
      stripServerStatus: localStorage.getItem('stripServerStatus') === 'true',
      stripLog: (JSON.parse(sessionStorage.getItem('stripLog')) as Array<String>) || ([] as Array<String>),
    }
  },
  actions: {
    setStripLog (payLoad: string) {
      if (this.stripLog.length >= 200) {
        this.stripLog.shift()
      }
      this.stripLog.push(payLoad)
      sessionStorage.setItem('stripLog', JSON.stringify(this.stripLog))
    },
    setStripClientStatus (payLoad: boolean) {
      this.stripClientStatus = payLoad
      setItem('stripClientStatus', JSON.stringify(payLoad))
    },
    setStripServerStatus (payLoad: boolean) {
      this.stripServerStatus = payLoad
      setItem('stripServerStatus', JSON.stringify(payLoad))
    },
    setStripClientIp (payLoad: string) {
      this.stripClientIp = payLoad
      setItem('stripClientIp', payLoad)
    },
    setStripClientPort (payLoad: number) {
      this.stripClientPort = payLoad
      setItem('stripClientPort', JSON.stringify(payLoad))
    },
    setStripServerPort (payLoad: number) {
      this.stripServerPort = payLoad
      setItem('stripServerPort', JSON.stringify(payLoad))
    },
  },
})
