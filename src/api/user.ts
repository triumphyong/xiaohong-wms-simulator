import request from '@/utils/request'
import { Result } from '@/types/result'
import { User } from '@/types/user'

export const login = (username: string, password: string) => {
  return request.post<Result<String>>('/data/user/login', { username, password })
}

export const logout = () => {
  return request.get<Result<String>>('/data/user/logout')
}

export const getUserInfo = () => {
  return request.get<Result<User>>('/data/user/userInfo')
}
