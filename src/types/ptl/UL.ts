import { PtlCommon } from './Common'
export interface UlMessage extends PtlCommon {
  r:string
  g:string
  b:string
  isButtonFlash:string
  isTextFlash:string
  isBuzzer:string
  comfirmBehaves:string
  isEnableMenu:string
  menuBehaves:string
  externalOutput1:string
  externalOutput2:string
  plusAndMinusBehaves:string
  data:string
  step:number
}
