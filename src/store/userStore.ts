import { User } from '@/types/user'
import { TOKEN, USER } from '@/utils/constants'
import { getItem, removeItem, setItem } from '@/utils/storage'
import { defineStore } from 'pinia'

export const userStore = defineStore('userStore', {
  state: () => {
    return {
      user: getItem<User>(USER),
      token: localStorage.getItem(TOKEN)
    }
  },
  // could also be defined as
  // state: () => ({ count: 0 })
  actions: {
    setUserInfo (payLoad:User|null) {
      this.user = payLoad
      setItem(USER, payLoad)
    },
    setToken (payLoad:string|null) {
      this.token = payLoad
      setItem(TOKEN, payLoad)
    },
    logout () {
      this.user = null
      this.token = null
      removeItem(TOKEN)
      removeItem(USER)
    }
  }

})
