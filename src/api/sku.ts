import request from '@/utils/request'
import { Result } from '@/types/result'
import { Page } from '@/types/page'
import { Sku } from '@/types/sku'

// 分页查询
export const getSkuPage = (page: number, size: number) => {
  return request.get<Result<Page<Sku>>>(`/data/sku/search/${page}/${size}`)
}

// 条件分页查询
export const getSkuSerchPage = (page: number, size: number, params: any) => {
  return request.post<Result<Page<Sku>>>(`/data/sku/search/${page}/${size}`)
}

// 添加Sku
export const addSku = (data: any) => {
  return request.post<Result<Sku>>('/data/sku', data)
}

// 更新sku
export const updateSku = (params: any) => {
  return request.put<Result<Sku>>('/data/sku', params)
}

// 根据Id删除sku
export const deleteSku = (id: number) => {
  return request.delete<Result<Sku>>(`/data/sku/${id}`)
}
