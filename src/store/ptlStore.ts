import { setItem } from '@/utils/storage'
import { defineStore } from 'pinia'

export const ptlStore = defineStore('ptlStore', {
  state: () => {
    return {
      ptlClientIp: localStorage.getItem('ptlClientIp') || '127.0.0.1',
      ptlClientPort: parseInt(localStorage.getItem('ptlClientPort')) || null,
      ptlServerPort: parseInt(localStorage.getItem('ptlServerPort')) || null,
      ptlClientStatus: localStorage.getItem('ptlClientStatus') === 'true',
      ptlServerStatus: localStorage.getItem('ptlServerStatus') === 'true',
      ptlLog: (JSON.parse(sessionStorage.getItem('ptlLog')) as Array<String>) || ([] as Array<String>),
    }
  },
  actions: {
    setPtlLog (payLoad: string) {
      if (this.ptlLog.length >= 200) {
        this.ptlLog.shift()
      }
      this.ptlLog.push(payLoad)
      sessionStorage.setItem('ptlLog', JSON.stringify(this.ptlLog))
    },
    setPtlClientStatus (payLoad: boolean) {
      this.ptlClientStatus = payLoad
      setItem('ptlClientStatus', JSON.stringify(payLoad))
    },
    setPtlServerStatus (payLoad: boolean) {
      this.ptlServerStatus = payLoad
      setItem('ptlServerStatus', JSON.stringify(payLoad))
    },
    setPtlClientIp (payLoad: string) {
      this.ptlClientIp = payLoad
      setItem('ptlClientIp', payLoad)
    },
    setPtlClientPort (payLoad: number) {
      this.ptlClientPort = payLoad
      setItem('ptlClientPort', JSON.stringify(payLoad))
    },
    setPtlServerPort (payLoad: number) {
      this.ptlServerPort = payLoad
      setItem('ptlServerPort', JSON.stringify(payLoad))
    },
  },
})
