import { PtlCommon } from './Common'
export interface KpMessage extends PtlCommon {
  confirmStatus:string
  minusStatus:string
  plusStatus:string
  menuStatus:string
  external1Status:string
  external2Status:string
  data:string
}
