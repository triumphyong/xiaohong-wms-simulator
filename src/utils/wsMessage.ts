/**
 * websocket消息处理
 */

import { plcStore } from '@/store/plcStore'
import { robokitStore } from '@/store/robokitStore'
import { generalStore } from '@/store/generalStore'
import { websocketStore } from '@/store/websocketStore'
import { agvServerStore } from '@/store/agvServerStore'
import { scanStore } from '@/store/scanStore'
import { ptlStore } from '@/store/ptlStore'
import { stripStore } from '@/store/stripStore'
import { roboStore } from '@/store/roboStore'
import { ikeaStore } from '@/store/ikeaStore'
import { ElMessage } from 'element-plus'
const plcstore = plcStore()
const robokitstore = robokitStore()
const generalstore = generalStore()
const websocketstore = websocketStore()
const agvServerstore = agvServerStore()
const scanstore = scanStore()
const ptlstore = ptlStore()
const stripstore = stripStore()
const robostore = roboStore()
const ikeastore = ikeaStore()

interface WsMessage {
  ip: string,
  port: number,
  type: number,
  message: string
}

export default function handleWsMessage (str: string) {
  const message = JSON.parse(str) as WsMessage

  switch (message.type) {
    case 5000:
      // PLC日志消息
      plcstore.setPlcLog(message.message)
      break
    case 5001:
      // plc客户端打开消息
      plcstore.setPlcLog(message.message)
      plcstore.setPlcClientStatus(true)
      break
    case 5002:
      // plc客户端关闭消息
      plcstore.setPlcLog(message.message)
      plcstore.setPlcClientStatus(false)
      break
    case 5003:
      // plc服务端打开消息
      plcstore.setPlcLog(message.message)
      plcstore.setPlcServerStatus(true)
      break
    case 5004:
      // plc服务端关闭消息
      plcstore.setPlcLog(message.message)
      plcstore.setPlcServerStatus(false)
      break
    case 5005:
      // robokit日志消息
      robokitstore.setRobokitLog(message.message)
      break
    case 5006:
      // robokit客户端打开消息
      robokitstore.setRobokitLog(message.message)
      robokitstore.setRobokitClientStatus(true)
      break
    case 5007:
      // robokit客户端关闭消息
      robokitstore.setRobokitLog(message.message)
      robokitstore.setRobokitClientStatus(false)
      break
    case 5008:
      // robokit服务端打开消息
      robokitstore.setRobokitLog(message.message)
      robokitstore.setRobokitServerStatus(true)
      break
    case 5009:
      // robokit服务端关闭消息
      robokitstore.setRobokitLog(message.message)
      robokitstore.setRobokitServerStatus(false)
      break
    case 5010:
      // general日志消息
      generalstore.setGeneralLog(message.message)
      break
    case 5011:
      // general客户端打开消息
      generalstore.setGeneralLog(message.message)
      generalstore.setGeneralClientStatus(true)
      break
    case 5012:
      // general客户端关闭消息
      generalstore.setGeneralLog(message.message)
      generalstore.setGeneralClientStatus(false)
      break
    case 5013:
      // general服务端打开消息
      generalstore.setGeneralLog(message.message)
      generalstore.setGeneralServerStatus(true)
      break
    case 5014:
      // general服务端关闭消息
      generalstore.setGeneralLog(message.message)
      generalstore.setGeneralServerStatus(false)
      break
    case 5015:
      // websocket日志消息
      websocketstore.setWebsocketLog(message.message)
      break
    case 5016:
      // websocket客户端打开消息
      websocketstore.setWebsocketLog(message.message)
      websocketstore.setWebsocketClientStatus(true)
      break
    case 5017:
      // websocket客户端关闭消息
      websocketstore.setWebsocketLog(message.message)
      websocketstore.setWebsocketClientStatus(false)
      break
    case 5018:
      // websocket服务端打开消息
      websocketstore.setWebsocketLog(message.message)
      websocketstore.setWebsocketServerStatus(true)
      break
    case 5019:
      // websocket服务端关闭消息
      websocketstore.setWebsocketLog(message.message)
      websocketstore.setWebsocketServerStatus(false)
      break
    case 5020:
      // agvServer日志消息
      agvServerstore.setAgvServerLog(message.message)
      break
    case 5021:
      // agvServer客户端打开消息
      agvServerstore.setAgvServerLog(message.message)
      agvServerstore.setAgvServerClientStatus(true)
      break
    case 5022:
      // agvServer客户端关闭消息
      agvServerstore.setAgvServerLog(message.message)
      agvServerstore.setAgvServerClientStatus(false)
      break
    case 5023:
      // agvServer服务端打开消息
      agvServerstore.setAgvServerLog(message.message)
      agvServerstore.setAgvServerServerStatus(true)
      break
    case 5024:
      // agvServer服务端关闭消息
      agvServerstore.setAgvServerLog(message.message)
      agvServerstore.setAgvServerServerStatus(false)
      break
    case 5025:
      // scan日志消息
      scanstore.setScanLog(message.message)
      break
    case 5026:
      // scan客户端打开消息
      scanstore.setScanLog(message.message)
      scanstore.setScanClientStatus(true)
      break
    case 5027:
      // scan客户端关闭消息
      scanstore.setScanLog(message.message)
      scanstore.setScanClientStatus(false)
      break
    case 5028:
      // scan服务端打开消息
      scanstore.setScanLog(message.message)
      scanstore.setScanServerStatus(true)
      break
    case 5029:
      // scan服务端关闭消息
      scanstore.setScanLog(message.message)
      scanstore.setScanServerStatus(false)
      break
    case 5030:
      // ptl日志消息
      ptlstore.setPtlLog(message.message)
      break
    case 5031:
      // ptl客户端打开消息
      ptlstore.setPtlLog(message.message)
      ptlstore.setPtlClientStatus(true)
      break
    case 5032:
      // ptl客户端关闭消息
      ptlstore.setPtlLog(message.message)
      ptlstore.setPtlClientStatus(false)
      break
    case 5033:
      // ptl服务端打开消息
      ptlstore.setPtlLog(message.message)
      ptlstore.setPtlServerStatus(true)
      break
    case 5034:
      // ptl服务端关闭消息
      ptlstore.setPtlLog(message.message)
      ptlstore.setPtlServerStatus(false)
      break
    case 5035:
      // strip日志消息
      stripstore.setStripLog(message.message)
      break
    case 5036:
      // strip客户端打开消息
      stripstore.setStripLog(message.message)
      stripstore.setStripClientStatus(true)
      break
    case 5037:
      // strip客户端关闭消息
      stripstore.setStripLog(message.message)
      stripstore.setStripClientStatus(false)
      break
    case 5038:
      // strip服务端打开消息
      stripstore.setStripLog(message.message)
      stripstore.setStripServerStatus(true)
      break
    case 5039:
      // strip服务端关闭消息
      stripstore.setStripLog(message.message)
      stripstore.setStripServerStatus(false)
      break
    case 5040:
      // robo日志消息
      robostore.setRoboLog(message.message)
      break
    case 5041:
      // robo客户端打开消息
      robostore.setRoboLog(message.message)
      robostore.setRoboClientStatus(true)
      break
    case 5042:
      // robo客户端关闭消息
      robostore.setRoboLog(message.message)
      robostore.setRoboClientStatus(false)
      break
    case 5043:
      // robo服务端打开消息
      robostore.setRoboLog(message.message)
      robostore.setRoboServerStatus(true)
      break
    case 5044:
      // robo服务端关闭消息
      robostore.setRoboLog(message.message)
      robostore.setRoboServerStatus(false)
      break
    case 5045:
      // ikea日志消息
      ikeastore.setIkeaLog(message.message)
      break
    case 5046:
      // ikea客户端打开消息
      ikeastore.setIkeaLog(message.message)
      ikeastore.setIkeaClientStatus(true)
      break
    case 5047:
      // ikea客户端关闭消息
      ikeastore.setIkeaLog(message.message)
      ikeastore.setIkeaClientStatus(false)
      break
    case 5048:
      // ikea服务端打开消息
      ikeastore.setIkeaLog(message.message)
      ikeastore.setIkeaServerStatus(true)
      break
    case 5049:
      // ikea服务端关闭消息
      ikeastore.setIkeaLog(message.message)
      ikeastore.setIkeaServerStatus(false)
      break
    default:
      // 其他消息
      ElMessage(message.message)
      break
  }
}
