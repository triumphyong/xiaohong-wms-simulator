export interface PtlCommon {
  direction: string
  address: string
  messageType: string
}
