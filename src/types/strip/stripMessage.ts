import { StripBody } from './stripBody'
export interface StripMessage {
  optionLayer: number,
  optionType: string,
  body?:Array<StripBody>,
  hbPoint?: number
 }
