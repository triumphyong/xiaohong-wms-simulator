export interface PlcMessage {
  acknowledgementRequired: boolean,
  payloadType: string,
  payload?: Object
 }
