import { createRouter, createWebHashHistory, createWebHistory, RouteRecordRaw, RouterView } from 'vue-router'
import Layout from '@/layout/index.vue'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css' // 注意要引入css样式文件
import { userStore } from '@/store/userStore'

const routes:RouteRecordRaw[] = [
  {
    path: '/',
    component: Layout,
    meta: {
      requiresAuth: true
    },
    redirect: '/plc',
    children: [
      {
        path: 'plc',
        name: 'plc',
        redirect: '/plc/connect',
        component: RouterView,
        meta: {
          title: 'plc'
        },
        children: [
          {
            path: 'connect',
            name: 'plcConnect',
            component: () => import('@/views/plc/client.vue'),
            meta: {
              title: 'plcConnect'
            }
          },
          {
            path: 'creat',
            name: 'plcCreat',
            component: () => import('@/views/plc/server.vue'),
            meta: {
              title: 'plcCreat'
            }
          }
        ]
      },
      {
        path: 'robokit',
        name: 'robokit',
        redirect: '/robokit/client',
        component: RouterView,
        meta: {
          title: 'robokit'
        },
        children: [
          {
            path: 'client',
            name: 'robokitClient',
            component: () => import('@/views/robokit/client.vue'),
            meta: {
              title: 'robokitClient'
            }
          },
          {
            path: 'server',
            name: 'robokitServer',
            component: () => import('@/views/robokit/server.vue'),
            meta: {
              title: 'robokitServer'
            }
          }
        ]
      },
      {
        path: 'agvServer',
        name: 'agvServer',
        redirect: '/agvServer/client',
        component: RouterView,
        meta: {
          title: 'agvServer'
        },
        children: [
          {
            path: 'client',
            name: 'agvServerClient',
            component: () => import('@/views/agvServer/client.vue'),
            meta: {
              title: 'agvServerClient'
            }
          },
          {
            path: 'server',
            name: 'agvServerServer',
            component: () => import('@/views/agvServer/server.vue'),
            meta: {
              title: 'agvServerServer'
            }
          }
        ]
      },
      {
        path: 'general',
        name: 'general',
        redirect: '/general/client',
        component: RouterView,
        meta: {
          title: 'general'
        },
        children: [
          {
            path: 'client',
            name: 'generalClient',
            component: () => import('@/views/general/client.vue'),
            meta: {
              title: 'generalClient'
            }
          },
          {
            path: 'server',
            name: 'generalServer',
            component: () => import('@/views/general/server.vue'),
            meta: {
              title: 'generalServer'
            }
          }
        ]
      },
      {
        path: 'scan',
        name: 'scan',
        redirect: '/scan/client',
        component: RouterView,
        meta: {
          title: 'scan'
        },
        children: [
          {
            path: 'client',
            name: 'scanClient',
            component: () => import('@/views/scan/client.vue'),
            meta: {
              title: 'scanClient'
            }
          },
          {
            path: 'server',
            name: 'scanServer',
            component: () => import('@/views/scan/server.vue'),
            meta: {
              title: 'scanServer'
            }
          }
        ]
      },
      {
        path: 'websocket',
        name: 'websocket',
        redirect: '/websocket/client',
        component: RouterView,
        meta: {
          title: 'websocket'
        },
        children: [
          {
            path: 'client',
            name: 'websocketClient',
            component: () => import('@/views/websocket/client.vue'),
            meta: {
              title: 'websocketClient'
            }
          },
          {
            path: 'server',
            name: 'websocketServer',
            component: () => import('@/views/websocket/server.vue'),
            meta: {
              title: 'websocketServer'
            }
          }
        ]
      },
      {
        path: 'ptl',
        name: 'ptl',
        redirect: '/ptl/client',
        component: RouterView,
        meta: {
          title: 'ptl'
        },
        children: [
          {
            path: 'client',
            name: 'ptlClient',
            component: () => import('@/views/ptl/client.vue'),
            meta: {
              title: 'ptlClient'
            }
          },
          {
            path: 'server',
            name: 'ptlServer',
            component: () => import('@/views/ptl/server.vue'),
            meta: {
              title: 'ptlServer'
            }
          }
        ]
      },
      {
        path: 'strip',
        name: 'strip',
        redirect: '/strip/client',
        component: RouterView,
        meta: {
          title: 'strip'
        },
        children: [
          {
            path: 'client',
            name: 'stripClient',
            component: () => import('@/views/strip/client.vue'),
            meta: {
              title: 'stripClient'
            }
          },
          // {
          //   path: 'server',
          //   name: 'stripServer',
          //   component: () => import('@/views/strip/server.vue'),
          //   meta: {
          //     title: 'stripServer'
          //   }
          // }
        ]
      },
      {
        path: 'robo',
        name: 'robo',
        redirect: '/robo/client',
        component: RouterView,
        meta: {
          title: 'robo'
        },
        children: [
          {
            path: 'client',
            name: 'roboClient',
            component: () => import('@/views/robo/client.vue'),
            meta: {
              title: 'roboClient'
            }
          },
          {
            path: 'server',
            name: 'roboServer',
            component: () => import('@/views/robo/server.vue'),
            meta: {
              title: 'roboServer'
            }
          }
        ]
      },
      {
        path: 'ikea',
        name: 'ikea',
        redirect: '/ikea/client',
        component: RouterView,
        meta: {
          title: 'ikea'
        },
        children: [
          {
            path: 'client',
            name: 'ikeaClient',
            component: () => import('@/views/ikea/client.vue'),
            meta: {
              title: 'ikeaClient'
            }
          },
          {
            path: 'server',
            name: 'ikeaServer',
            component: () => import('@/views/ikea/server.vue'),
            meta: {
              title: 'ikeaServer'
            }
          }
        ]
      }
    ]
  }

]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

// 前置守卫
router.beforeEach((to, from) => {
  const store = userStore()
  NProgress.start() // 进度条开始
  // 而不是去检查每条路由记录
  // to.matched.some(record => record.meta.requiresAuth)
  // if (to.meta.requiresAuth && !store.token) {
  //   // 此路由需要授权，请检查是否已登录
  //   // 如果没有，则重定向到登录页面
  //   return {
  //     path: '/login',
  //     // 保存我们所在的位置，以便以后再来
  //     query: { redirect: to.fullPath }
  //   }
  // }
})

// 后置守卫
router.afterEach(() => {
  NProgress.done() // 进度条结束
})

// 进度条的配置项：ease可以设置css3动画，如ease，linear；speed是进度条从开始到结束的耗时
NProgress.configure({ easing: 'linear', speed: 500 })

export default router
