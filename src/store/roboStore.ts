import { setItem } from '@/utils/storage'
import { defineStore } from 'pinia'

export const roboStore = defineStore('roboStore', {
  state: () => {
    return {
      roboClientIp: localStorage.getItem('roboClientIp') || '127.0.0.1',
      roboClientPort: parseInt(localStorage.getItem('roboClientPort')) || null,
      roboServerPort: parseInt(localStorage.getItem('roboServerPort')) || null,
      roboClientStatus: localStorage.getItem('roboClientStatus') === 'true',
      roboServerStatus: localStorage.getItem('roboServerStatus') === 'true',
      roboLog: JSON.parse(sessionStorage.getItem('roboLog')) as Array<String> || [] as Array<String>
    }
  },
  actions: {
    setRoboLog (payLoad:string) {
      if (this.roboLog.length >= 200) {
        this.roboLog.shift()
      }
      this.roboLog.push(payLoad)
      sessionStorage.setItem('roboLog', JSON.stringify(this.roboLog))
    },
    setRoboClientStatus (payLoad:boolean) {
      this.roboClientStatus = payLoad
      setItem('roboClientStatus', JSON.stringify(payLoad))
    },
    setRoboServerStatus (payLoad:boolean) {
      this.roboServerStatus = payLoad
      setItem('roboServerStatus', JSON.stringify(payLoad))
    },
    setRoboClientIp (payLoad:string) {
      this.roboClientIp = payLoad
      setItem('roboClientIp', payLoad)
    },
    setRoboClientPort (payLoad:number) {
      this.roboClientPort = payLoad
      setItem('roboClientPort', JSON.stringify(payLoad))
    },
    setRoboServerPort (payLoad:number) {
      this.roboServerPort = payLoad
      setItem('roboServerPort', JSON.stringify(payLoad))
    }
  }
})
