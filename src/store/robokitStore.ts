import { setItem } from '@/utils/storage'
import { defineStore } from 'pinia'

export const robokitStore = defineStore('robokitStore', {
  state: () => {
    return {
      robokitClientIp: localStorage.getItem('robokitClientIp') || '127.0.0.1',
      robokitClientPort: parseInt(localStorage.getItem('robokitClientPort')) || null,
      robokitServerPort: parseInt(localStorage.getItem('robokitServerPort')) || null,
      robokitClientStatus: localStorage.getItem('robokitClientStatus') === 'true',
      robokitServerStatus: localStorage.getItem('robokitServerStatus') === 'true',
      robokitLog: JSON.parse(sessionStorage.getItem('robokitLog')) as Array<String> || [] as Array<String>
    }
  },
  actions: {
    setRobokitLog (payLoad:string) {
      if (this.robokitLog.length >= 200) {
        this.robokitLog.shift()
      }
      this.robokitLog.push(payLoad)
      sessionStorage.setItem('robokitLog', JSON.stringify(this.robokitLog))
    },
    setRobokitClientStatus (payLoad:boolean) {
      this.robokitClientStatus = payLoad
      setItem('robokitClientStatus', JSON.stringify(payLoad))
    },
    setRobokitServerStatus (payLoad:boolean) {
      this.robokitServerStatus = payLoad
      setItem('robokitServerStatus', JSON.stringify(payLoad))
    },
    setRobokitClientIp (payLoad:string) {
      this.robokitClientIp = payLoad
      setItem('robokitClientIp', payLoad)
    },
    setRobokitClientPort (payLoad:number) {
      this.robokitClientPort = payLoad
      setItem('robokitClientPort', JSON.stringify(payLoad))
    },
    setRobokitServerPort (payLoad:number) {
      this.robokitServerPort = payLoad
      setItem('robokitServerPort', JSON.stringify(payLoad))
    }
  }
})
