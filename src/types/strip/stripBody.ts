export interface StripBody {
  start: number,
  end: number,
  r:string,
  g:string,
  b:string
 }
